package com.bidkraft.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "priceline")
public class PriceLine {

	private long pricelineid;
	private int requestid;
	private int priceLineType;
	private double amount;
	private int currencyType;
	
	@Id
	@GeneratedValue
	@Column(name="id",unique=true)
	public long getPricelineid() {
		return pricelineid;
	}

	public void setPricelineid(long pricelineid) {
		this.pricelineid = pricelineid;
	}


	@Column(name = "request_id")
	public int getRequestid() {
		return requestid;
	}

	public void setRequestid(int requestid) {
		this.requestid = requestid;
	}

	@Column(name = "priceline_type")
	public int getPriceLineType() {
		return priceLineType;
	}

	public void setPriceLineType(int priceLineType) {
		this.priceLineType = priceLineType;
	}

	@Column(name = "amount")
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Column(name = "currency_type")
	public int getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(int currencyType) {
		this.currencyType = currencyType;
	}

}
