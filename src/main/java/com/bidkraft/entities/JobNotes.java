package com.bidkraft.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity 
@Table(name="job")
public class JobNotes {
	
	private int jobnotesid;
	private long parentid;
	private int userid;
	private String note;
	
	@Id
	@GeneratedValue
	@Column(name="id",unique=true)
	public int getJobnotesid() {
		return jobnotesid;
	}
	public void setJobnotesid(int jobnotesid) {
		this.jobnotesid = jobnotesid;
	}
	public long getParentid() {
		return parentid;
	}
	public void setParentid(long parentid) {
		this.parentid = parentid;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}


}
