package com.bidkraft.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;

@Entity
@Table(name="user")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
public class User {
	
	@JsonProperty("userid")
	private long userid;
	
	@JsonProperty("email")
	private String email;
	
	@JsonIgnore
	private String password;
	
	@JsonProperty("lat")
	private double latitude;
	
	@JsonProperty("lon")
	private double longitude;
	
	@JsonProperty("bgcstatus")
	private String bgcstatus;
	
	@JsonProperty("rating")
	private double rating;
	
	@JsonProperty("image")
    private byte[] image;
	
	@JsonProperty("iostoken")
	private String iOSToken;
	
	@JsonProperty("andriodtoken")
	private String andriodToken;
    
    public User() {
    	
    }
    
    //Copy Constructor decator pattern
    public User(User u){
    	this.userid = u.getUserid();
    	this.email = u.getEmail();
    	this.latitude = u.getLatitude();
    	this.longitude = u.getLongitude();
    	this.bgcstatus = u.getBgcstatus();
    	this.rating = u.getRating();
    	this.image = u.getImage();
    	this.iOSToken = u.getiOSToken();
    	this.andriodToken = u.getAndriodToken();
    }
    
	
	@Id
	@GeneratedValue
	@Column(name="userid",unique=true)
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	
	@Column(name="email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="password")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Column(name="lat")
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	@Column(name="lon")
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	@Column(name="bgc_status")
	public String getBgcstatus() {
		return bgcstatus;
	}
	public void setBgcstatus(String bgcstatus) {
		this.bgcstatus = bgcstatus;
	}
	
	@Column(name="rating")
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	
	@Column(name="ios_token")
	public String getiOSToken() {
		return iOSToken;
	}
	
	public void setiOSToken(String iOSToken) {
		this.iOSToken = iOSToken;
	}
	
	@Column(name="andriod_token")
	public String getAndriodToken() {
		return andriodToken;
	}
	
	public void setAndriodToken(String andriodToken) {
		this.andriodToken = andriodToken;
	}
	
	@Lob
    @Column(name="profile_image", nullable=true, columnDefinition="mediumblob")
	public byte[] getImage() {
        return image;
    }
 
    public void setImage(byte[] image) {
        this.image = image;
    }
}
