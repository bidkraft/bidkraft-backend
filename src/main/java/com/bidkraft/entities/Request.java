package com.bidkraft.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@Entity
@Table(name = "request")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Request  {

	@JsonProperty("requestid")
	private int requestid;
	
	@JsonProperty("title")
	private String title;
	
	@JsonProperty("description")
	private String description;

	@JsonProperty("userid")
	private long userid;

	@JsonProperty("start_date")
	private Date start_date;
	
	@JsonProperty("end_date")
	private Date end_date;

	@JsonProperty("lat")
	private double lat;
	
	@JsonProperty("lon")
	private double lon;
	
	@JsonProperty("address")
	private String address;

	@JsonProperty("status")
	private int status;

	@JsonProperty("summary")
	private String summary;
	
	@JsonProperty("category")
	private String category;
	
	@JsonProperty("jobid")
	private long jobid;
	
	@JsonProperty("winningbidid")
	private int winningbidid;

	public Request() {

	}

	@Id
	@GeneratedValue
	@Column(name = "id", unique = true)
	public int getRequestid() {
		return requestid;
	}

	public void setRequestid(int requestid) {
		this.requestid = requestid;
	}

	@Column(name = "title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "userid")
	public long getUserId() {
		return userid;
	}

	public void setUserId(long userid) {
		this.userid = userid;
	}
	
	@Column(name = "jobid")
	public long getJobId() {
		return jobid;
	}

	public void setJobId(long jobid) {
		this.jobid = jobid;
	}

	@Column(name = "start_date")
	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	@Column(name = "end_date")
	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	@Column(name = "lat")
	public double getLatitude_request() {
		return lat;
	}

	public void setLatitude_request(double latitude_request) {
		this.lat = latitude_request;
	}

	@Column(name = "lon")
	public double getLongitude_request() {
		return lon;
	}

	public void setLongitude_request(double longitude_request) {
		this.lon = longitude_request;
	}

	@Column(name = "address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;

	}

	@Column(name = "status")
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "summary")
	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	@Column(name = "category")
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	@Column(name="winningbidid")
	public int getWinningbidid() {
		return winningbidid;
	}

	public void setWinningbidid(int winningbidid) {
		this.winningbidid = winningbidid;
	}

}
