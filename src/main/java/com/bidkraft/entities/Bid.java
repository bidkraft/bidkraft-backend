package com.bidkraft.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;

@Entity
@Table(name="bid")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
public class Bid implements Serializable {
	
	@JsonIgnore
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("bidid")
	private int bidid;
	
	@JsonProperty("requestid")
	private int requestId;
	
	@JsonProperty("description")
	private String description;	
	
	@JsonProperty("amount")
	private double amount;	
	
	@JsonProperty("currency")
	private int currency;
	
	@JsonProperty("userid")
	private long userId;
	
	@JsonProperty("status")
	private int status;
	
	@JsonProperty("biddate")
	private Date bidDate;
	
	public Bid() {
		
	}
	
	//Decorator Pattern
	public Bid(Bid b){
		this.bidid = b.getBidid();
		this.requestId = b.getRequestId();
		this.description = b.getDescription();
		this.currency = b.getCurrency();
		this.amount = b.getAmount();
		this.userId = b.getUserId();
		this.status = b.getStatus();
		this.bidDate = b.getBidDate();
	}
	
	
	@Id
	@GeneratedValue
	@Column(name="id")
	public int getBidid() {
		return bidid;
	}
	public void setBidid(int bidid) {
		this.bidid = bidid;
	}
	
	@Column(name="request_id")
	public int getRequestId() {
		return requestId;
	}
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}
	
	@Column(name="description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name="currency")
	public int getCurrency() {
		return currency;
	}
	
	public void setCurrency(int currency) {
		this.currency = currency;
	}
	
	@Column(name="amount")
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	@Column(name="user_id")
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	@Column(name="status")
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name="bid_date", columnDefinition="DATETIME")
	public Date getBidDate(){
		return bidDate;
	}

	public void setBidDate(Date bidDate){
		this.bidDate = bidDate;
	}
	
}
