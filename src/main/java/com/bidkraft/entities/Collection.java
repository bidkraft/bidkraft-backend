package com.bidkraft.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "collection")
public class Collection {

	private int collectionid;
	private long jobid;
	private int buyerid;
	private double amount;
	private int currencytype;
	private int collectionstatus;
	private String transactionid;
	
	
	@Id
	@GeneratedValue
	@Column(name="id",unique=true)
	public int getCollectionid() {
		return collectionid;
	}
	public void setCollectionid(int collectionid) {
		this.collectionid = collectionid;
	}
	
	@Column(name="job_id")
	public long getJobid() {
		return jobid;
	}
	public void setJobid(long jobid) {
		this.jobid = jobid;
	}
	
	@Column(name="buyer_id")
	public int getBuyerid() {
		return buyerid;
	}
	public void setBuyerid(int buyerid) {
		this.buyerid = buyerid;
	}
	
	@Column(name="amount")
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	@Column(name="currency_type")
	public int getCurrencytype() {
		return currencytype;
	}
	public void setCurrencytype(int currencytype) {
		this.currencytype = currencytype;
	}
	
	@Column(name="collection_status")
	public int getCollectionstatus() {
		return collectionstatus;
	}
	public void setCollectionstatus(int collectionstatus) {
		this.collectionstatus = collectionstatus;
	}
	
	@Column(name="transaction_id")
	public String getTransactionid() {
		return transactionid;
	}
	public void setTransactionid(String transactionid) {
		this.transactionid = transactionid;
	}

}
