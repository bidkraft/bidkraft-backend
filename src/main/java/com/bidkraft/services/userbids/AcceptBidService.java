package com.bidkraft.services.userbids;

import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.Bid;
import com.bidkraft.entities.Job;
import com.bidkraft.entities.PriceLine;
import com.bidkraft.enums.BidStatus;
import com.bidkraft.enums.JobStatus;
import com.bidkraft.enums.PriceLineType;
import com.bidkraft.enums.RequestStatus;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.services.KraftService;
import com.bidkraft.sync.SyncElasticsearch;

@Component
@SuppressWarnings("unchecked")
public class AcceptBidService implements KraftService<String> {
	
	@Autowired
	SyncElasticsearch syncElasticsearch;

	@Override
	public String service(KraftRequest request) throws KraftException {
		
		if (request.getEntities().containsKey(ServiceKeys.ABID)) {
			Map<String, String> usermap = (Map<String, String>) request.getEntities().get(ServiceKeys.ABID);

			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction transcation = session.beginTransaction();
			
			int bidId = Integer.valueOf(usermap.get("bidid"));
			int reqId = Integer.valueOf(usermap.get("reqid"));
			
			
			Query query = session.createQuery("from Bid where id= :bid");
			query.setParameter("bid", bidId);
			List<Bid> bidEntities = query.list();
			
			if(bidEntities.size() > 0) {
			double bidAmount = bidEntities.get(0).getAmount();
			double fees = (bidAmount * 5)/100;
			double totalAmount = bidAmount + fees;
			try {

				// Update bid
				Query query1 = session.createQuery("update Bid set status = :status" + " where id = :bidid");
				query1.setParameter("status", BidStatus.WINING_BID.getId());
				query1.setParameter("bidid", bidId);
				query1.executeUpdate();

				// Update Request
				Query query2 = session.createQuery("update Request set status = :status,"+ " winningbidid = :winningbidid"  + " where id = :reqid");
				query2.setParameter("status", RequestStatus.AWAITING_PAYMENT.getId());
				query2.setParameter("winningbidid", bidId);
				query2.setInteger("reqid", reqId);
				query2.executeUpdate();
				
				// Update Priceline
				PriceLine itemCostObj = new PriceLine();
				itemCostObj.setAmount(bidAmount);
				itemCostObj.setRequestid(reqId);
				itemCostObj.setPriceLineType(PriceLineType.ITEM_COST.getId());
				session.save(itemCostObj);
				
				PriceLine feesObj = new PriceLine();
				feesObj.setAmount(fees);
				feesObj.setRequestid(reqId);
				feesObj.setPriceLineType(PriceLineType.FEES.getId());
				session.save(feesObj);
				
				PriceLine totalAmountObj = new PriceLine();
				totalAmountObj.setAmount(totalAmount);
				totalAmountObj.setRequestid(reqId);
				totalAmountObj.setPriceLineType(PriceLineType.TOTAL_AMOUNT.getId());
				session.save(totalAmountObj);
				
				//Write to Elasticsearch
				syncElasticsearch.writeToES(reqId);
				transcation.commit();
			} catch (Exception e) {
				transcation.rollback();
				throw new KraftException("Exception in GetRequestInfoService " + e.getMessage());
				//System.out.println(e.getMessage());
			} finally {
				// Commit
				session.flush();
			}
			} else {
				return "{\"error\": \"Please check bidId\"}";
			}
		}
		return "";
	}
}
