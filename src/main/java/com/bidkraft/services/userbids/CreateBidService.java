package com.bidkraft.services.userbids;

import java.util.Map;

import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.Bid;
import com.bidkraft.enums.BidStatus;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.services.KraftService;
import com.bidkraft.util.KraftUtil;

@Component
@SuppressWarnings("unchecked")
public class CreateBidService implements KraftService<Bid> {

	@Override
	public Bid service(KraftRequest request) {
		Bid bidobj = new Bid();
		if (request.getEntities().containsKey(ServiceKeys.CBID)) {

			Map<String, String> bidmap = (Map<String, String>) request.getEntities().get(ServiceKeys.CBID);
			System.out.println("This is bid service:");
			bidobj.setRequestId(Integer.valueOf(bidmap.get("requestid")));
			bidobj.setDescription(bidmap.get("description"));
			bidobj.setAmount(Double.parseDouble(bidmap.get("amount")));
			bidobj.setUserId(Long.parseLong(bidmap.get("userid")));
			bidobj.setBidDate(KraftUtil.getSQLDate());
			bidobj.setStatus(BidStatus.ACTIVE_BID.getId());

			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(bidobj);

			System.out.println("Generated bid id:" + bidobj.getBidid());
			session.getTransaction().commit();
			session.flush();
		}
		return bidobj;
	}
}