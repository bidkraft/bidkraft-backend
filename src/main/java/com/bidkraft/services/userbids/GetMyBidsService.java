package com.bidkraft.services.userbids;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.Bid;
import com.bidkraft.enums.RequestStatus;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.responses.MyBidsResponse;
import com.bidkraft.responses.UserRequestResponse;
import com.bidkraft.services.KraftService;
import com.bidkraft.services.userrequests.GetRequestInfoService;

@Component
@SuppressWarnings("unchecked")
public class GetMyBidsService implements KraftService<MyBidsResponse> {

	@Autowired
	GetRequestInfoService requestInfoService;

	@Override
	public MyBidsResponse service(KraftRequest request) throws KraftException {
		MyBidsResponse result = new MyBidsResponse();
		List<Bid> userBids = new ArrayList<Bid>();
		if (request.getEntities().containsKey(ServiceKeys.GETUSERBIDS)) {
			Map<String, String> requestMap = (Map<String, String>) request.getEntities().get(ServiceKeys.GETUSERBIDS);

			Long userid = Long.parseLong(requestMap.get("userid"));
			Session session = HibernateUtil.getSessionFactory().openSession();
			Query query = session.createQuery("from Bid where userId= :userid");
			query.setParameter("userid", userid);

			userBids = query.list();

			if (userBids != null) {
				for (Bid b : userBids) {
					UserRequestResponse response = requestInfoService.getRequestInfo(String.valueOf(b.getRequestId()),
							userid);
					// Show only requests the user bid for. Not his/her own.
					if (response.getRequest().getUserId() != userid) {
						if (response.request.getStatus() == RequestStatus.WAITING_FOR_BIDS.getId()) {
							result.addActiveBids(response);

						} else if (response.request.getStatus() == RequestStatus.AWAITING_PAYMENT.getId()
								|| response.request.getStatus() == RequestStatus.PAID.getId()) {
							result.addAcceptedBids(response);

						} else if (response.request.getStatus() == RequestStatus.COMPLETED.getId()) {
							result.addCompletedBids(response);

						}
					}
				}
			}
		}
		return result;
	}

}
