package com.bidkraft.services.userrequests;

import java.util.List;
import java.util.concurrent.Callable;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bidkraft.elasticsearch.ElasticsearchAPIFactory;
import com.bidkraft.elasticsearch.FinderAPI;
import com.bidkraft.elasticsearch.IndexAPI;
import com.bidkraft.entities.User;
import com.bidkraft.exception.KraftException;
import com.bidkraft.filters.GeoQuery;
import com.bidkraft.filters.QueryGroup;
import com.bidkraft.pushnotifications.PushNotificationService;
import com.bidkraft.services.user.GetUserDetails;
import com.bidkraft.util.JsonMapper;

@Component
public class PushToUsersCloseBy implements Callable<Void> {
	
	@Autowired
	GetUserDetails userDetailsProvider;
	
	@Autowired
	IndexAPI indexAPI;
	
	@Autowired
	JsonMapper jsonMapper;
	
	@Autowired
	PushNotificationService pushNotificationService;
	
	private Long userId;
	private String title;
	private String body;
	
	public void setTitle(String title) {
		this.title = title;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	public Void call() throws Exception {
		push(this.userId, this.title, this.body);
		return null;
	}
	
	public void push(Long userId, String title, String body) throws KraftException {	
		User user = userDetailsProvider.getUser(userId);	
		FinderAPI finderAPI = ElasticsearchAPIFactory.getFinderAPI();		
		QueryGroup qg = new QueryGroup();
		GeoQuery geoQuery = new GeoQuery();
		geoQuery.distance = 50;
		geoQuery.latitude  = user.getLatitude();
		geoQuery.longitude = user.getLongitude();
		qg.addQuery(geoQuery);
		
		List<User> users = finderAPI.getResult(qg.getQuery(), "user", 0, 100,User.class);
		for(User userObj : users)	{
			if(StringUtils.isNotBlank(userObj.getiOSToken())){
				pushNotificationService.pushToiOS(user.getiOSToken(), title, body);
			}
			
			if(StringUtils.isNotBlank(userObj.getAndriodToken())){
				pushNotificationService.pushToAndriod(user.getAndriodToken(), title, body);
			}
		}
		
	}
	
}

