package com.bidkraft.services.userrequests;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import org.apache.commons.lang.StringEscapeUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.elasticsearch.IndexAPI;
import com.bidkraft.entities.Request;
import com.bidkraft.enums.RequestStatus;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.services.KraftService;
import com.bidkraft.util.JsonMapper;
import com.bidkraft.util.KraftUtil;
import com.google.common.base.Joiner;

@Component
public class CreateRequestService implements KraftService<Request> {
	
	@Autowired
	IndexAPI indexAPI;
	
	@Autowired
	JsonMapper jsonMapper;
	
	@Autowired
	PushToUsersCloseBy pushToUsersCloseBy;

	@Override
	public Request service(KraftRequest request) throws KraftException {
		Request reqobj = new Request();
		ObjectMapper mapper = new ObjectMapper();
		if (request.getEntities().containsKey(ServiceKeys.CREQ)) {
			@SuppressWarnings("unchecked")
			Map<String, Object> usermap = (Map<String, Object>) request.getEntities().get(ServiceKeys.CREQ);

			if (usermap.get("userid") != null) {
				reqobj.setUserId(Integer.parseInt(usermap.get("userid").toString()));
			}
			if (usermap.get("title") != null) {
				reqobj.setTitle(usermap.get("title").toString());
			}
			if (usermap.get("description") != null) {
				reqobj.setDescription(usermap.get("description").toString());
			}
			if (usermap.get("start_date") != null) {
				reqobj.setStart_date(KraftUtil.getSQLDate(usermap.get("start_date").toString()));
			}
			if (usermap.get("end_date") != null) {
				reqobj.setEnd_date(KraftUtil.getSQLDate(usermap.get("end_date").toString()));
			}
			if (usermap.get("lat") != null) {
				reqobj.setLatitude_request(Double.parseDouble(usermap.get("lat").toString()));
			}
			if (usermap.get("lon") != null) {
				reqobj.setLongitude_request(Double.parseDouble(usermap.get("lon").toString()));
			}
			if (usermap.get("address") != null) {
				reqobj.setAddress(usermap.get("address").toString());
			}
			if (usermap.get("summary") != null) {
				try {
					reqobj.setSummary(StringEscapeUtils.escapeJava(mapper.writeValueAsString(usermap.get("summary"))));
					usermap.put("summary", StringEscapeUtils.escapeJava(mapper.writeValueAsString(usermap.get("summary"))));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (usermap.get("category") != null) {
				reqobj.setCategory(usermap.get("category").toString());
			}

			reqobj.setStatus(RequestStatus.WAITING_FOR_BIDS.getId());
			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(reqobj);


			try {
				usermap.put("requestid", String.valueOf(reqobj.getRequestid()));
				usermap.put("status", RequestStatus.WAITING_FOR_BIDS.getId());
				if (reqobj.getLatitude_request() != 0 && reqobj.getLongitude_request() != 0) {
					usermap.put("location",
							Joiner.on(",").join(reqobj.getLatitude_request(), reqobj.getLongitude_request()));
				}
				indexAPI.indexRequest(String.valueOf(reqobj.getRequestid()), jsonMapper.getObjectMapper().writeValueAsString(usermap));
			} catch (KraftException e) {
				throw e;
			} catch (Exception e) {
				throw new KraftException(e.getMessage());
			}
			System.out.println("Generated request id:" + reqobj.getRequestid());
			session.getTransaction().commit();
			
			
			//Blocking 
			//pushToUsersCloseBy.push(reqobj.getUserId(), reqobj.getTitle(), reqobj.getDescription());
			
			//Fire and forget (Non Blocking)
			pushToUsersCloseBy.setUserId(reqobj.getUserId());
			pushToUsersCloseBy.setBody(reqobj.getDescription());
			pushToUsersCloseBy.setTitle(reqobj.getTitle());			
			FutureTask<Void> futureTask1 = new FutureTask<Void>(pushToUsersCloseBy);
			ExecutorService executor = Executors.newFixedThreadPool(1);
			executor.execute(futureTask1);
			
		}
		return reqobj;
	}
}
