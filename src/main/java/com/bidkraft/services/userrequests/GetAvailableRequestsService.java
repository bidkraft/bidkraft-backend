package com.bidkraft.services.userrequests;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.elasticsearch.ElasticsearchAPIFactory;
import com.bidkraft.elasticsearch.FinderAPI;
import com.bidkraft.entities.Request;
import com.bidkraft.exception.KraftException;
import com.bidkraft.filters.Query;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.responses.UserBid;
import com.bidkraft.responses.UserRequestResponse;
import com.bidkraft.services.KraftService;

/**
 * TotalBids, BestBid, Title, Description, Request-StartDate
 * @author Abhishek
 *
 */

@Component
public class GetAvailableRequestsService implements KraftService<List<UserRequestResponse>> {

	@Override
	public List<UserRequestResponse> service(KraftRequest request) throws KraftException {

		List<UserRequestResponse> requests = new ArrayList<UserRequestResponse>();
		try {
			if (request.getEntities().containsKey(ServiceKeys.GETAVAILABLEREQUESTS)) {
				@SuppressWarnings("unchecked")
				Map<String, Object> requestMap = (Map<String, Object>) request.getEntities()
						.get(ServiceKeys.GETAVAILABLEREQUESTS);

				FinderAPI finderAPI = ElasticsearchAPIFactory.getFinderAPI();

				@SuppressWarnings("unchecked")
				Map<String, Object> filters = (Map<String, Object>) requestMap.get("filter");

				ObjectMapper mapper = new ObjectMapper();
				
				Query query = mapper.convertValue(filters, Query.class);

				List<Request> response = finderAPI.getResult(query.getQuery(),"request", 0, 100, Request.class);
				GetRequestBidsService bidsService = new GetRequestBidsService();

				for (Request req : response) {
					List<UserBid> bids = bidsService.getUserBids(req.getRequestid(), null);
					requests.add(new UserRequestResponse(req.getRequestid(), req, bids));
				}
			}
		} catch (Exception e) {
			throw new KraftException(e.getMessage());
		}
		return requests;
	}

}
