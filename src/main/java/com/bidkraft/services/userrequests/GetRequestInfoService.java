package com.bidkraft.services.userrequests;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.elasticsearch.ElasticsearchAPIFactory;
import com.bidkraft.elasticsearch.FinderAPI;
import com.bidkraft.entities.Job;
import com.bidkraft.entities.Request;
import com.bidkraft.enums.BidStatus;
import com.bidkraft.enums.RequestStatus;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.responses.UserBid;
import com.bidkraft.responses.UserRequestResponse;
import com.bidkraft.services.KraftService;
import com.bidkraft.services.user.GetUserDetails;
import com.bidkraft.services.userjobs.GetUserJobsService;

@Component
public class GetRequestInfoService implements KraftService<UserRequestResponse> {

	@Autowired
	GetUserDetails userDetailsProvider;
	
	@Autowired
	GetRequestBidsService bidsService;
	
	@Autowired
	GetUserJobsService jobService;

	@SuppressWarnings("unchecked")
	@Override
	public UserRequestResponse service(KraftRequest request) throws KraftException {
		UserRequestResponse result = null;
		try {
			if (request.getEntities().containsKey(ServiceKeys.GETREQUESTINFO)) {
				Map<String, Object> requestMap = (Map<String, Object>) request.getEntities()
						.get(ServiceKeys.GETREQUESTINFO);

				result = getRequestInfo(requestMap.get("requestId").toString());
			}
		} catch (Exception ex) {
			throw new KraftException("Exception in GetRequestInfoService " + ex.getMessage());
		}

		// pushNotificationService.push("1", "Test", "Test");

		return result;
	}

	public UserRequestResponse getRequestInfo(String requestId) throws KraftException {
		UserRequestResponse result;
		FinderAPI finderAPI = ElasticsearchAPIFactory.getFinderAPI();

		Request req = finderAPI.getRequest(requestId);

		BidStatus bidStatus = (req.getStatus() == RequestStatus.PAID.getId()
				|| req.getStatus() == RequestStatus.AWAITING_PAYMENT.getId()
				|| req.getStatus() == RequestStatus.COMPLETED.getId()) ? BidStatus.WINING_BID : null;

		List<UserBid> bids = bidsService.getUserBids(req.getRequestid(), bidStatus);
		Job job = null;

		if (req.getStatus() == RequestStatus.PAID.getId() || req.getStatus() == RequestStatus.COMPLETED.getId()) {
			job = jobService.getJobById(req.getJobId()).get(0);
		}

		result = new UserRequestResponse(req.getRequestid(), req, bids, job);
		return result;
	}

	public UserRequestResponse getRequestInfo(String requestId, long userId) throws KraftException {
		UserRequestResponse result;
		FinderAPI finderAPI = ElasticsearchAPIFactory.getFinderAPI();

		Request req = finderAPI.getRequest(requestId);
		

		List<UserBid> bids = bidsService.getUserBids(req.getRequestid(), null);

		List<UserBid> userBids = new ArrayList<UserBid>();
		for (UserBid bid : bids) {
			if (bid.getUserId() == userId) {
				userBids.add(bid);
			}
		}
		bids = userBids;

		Job job = null;

		if (req.getStatus() == RequestStatus.PAID.getId() || req.getStatus() == RequestStatus.COMPLETED.getId()) {
			job = jobService.getJobById(req.getJobId()).get(0);
		}

		result = new UserRequestResponse(req.getRequestid(), req, bids, job);
		return result;
	}

	public UserRequestResponse getRequestInfo(Request req) throws KraftException {
		UserRequestResponse result;
		GetRequestBidsService bidsService = new GetRequestBidsService();
		GetUserJobsService jobService = new GetUserJobsService();

		BidStatus bidStatus = (req.getStatus() == RequestStatus.PAID.getId()
				|| req.getStatus() == RequestStatus.AWAITING_PAYMENT.getId()
				|| req.getStatus() == RequestStatus.COMPLETED.getId()) ? BidStatus.WINING_BID : null;

		List<UserBid> bids = bidsService.getUserBids(req.getRequestid(), bidStatus);
		Job job = null;

		if (req.getStatus() == RequestStatus.PAID.getId() || req.getStatus() == RequestStatus.COMPLETED.getId()) {
			job = jobService.getJobById(req.getJobId()).get(0);
		}

		result = new UserRequestResponse(req.getRequestid(), req, bids, job);
		return result;
	}

}
