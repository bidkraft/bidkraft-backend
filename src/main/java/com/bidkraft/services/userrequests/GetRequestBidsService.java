package com.bidkraft.services.userrequests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.Bid;
import com.bidkraft.entities.User;
import com.bidkraft.enums.BidStatus;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.responses.BidsResponse;
import com.bidkraft.responses.UserBid;
import com.bidkraft.services.KraftService;

@Component
public class GetRequestBidsService implements KraftService<BidsResponse> {

	@Override
	public BidsResponse service(KraftRequest request) throws KraftException {
		BidsResponse userBids = new BidsResponse();
		if (request.getEntities().containsKey(ServiceKeys.GETREQUESTBIDS)) {
			@SuppressWarnings("unchecked")
			Map<String, String> requestMap = (Map<String, String>) request.getEntities()
					.get(ServiceKeys.GETREQUESTBIDS);

			Long rid = Long.parseLong(requestMap.get("reqid"));
			userBids = getBids(rid, null);
		}
		return userBids;
	}

	@SuppressWarnings("unchecked")
	public BidsResponse getBids(Long requestId, BidStatus bidStatus) {
		BidsResponse userBids = new BidsResponse();
		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = null;
		if (bidStatus != null) {
			query = session.createQuery("from Bid where requestId= :rid and status= :status");
			query.setParameter("rid", requestId);
			query.setParameter("status", bidStatus.getId());
		} else {
			query = session.createQuery("from Bid where requestId= :rid");
			query.setParameter("rid", requestId);
		}

		List<Bid> bidEntities = query.list();

		if (bidEntities != null && bidEntities.size() > 0) {
			List<UserBid> bids = new ArrayList<UserBid>();

			Map<Long, User> bidMap = new HashMap<Long, User>();

			for (Bid b : bidEntities) {
				bidMap.put(b.getUserId(), null);
			}

			Query userQuery = session.createQuery("from User where userid in (:uid)");
			userQuery.setParameterList("uid", Arrays.asList(bidMap.keySet().toArray()));
			List<User> users = userQuery.list();

			for (User u : users) {
				bidMap.put(u.getUserid(), u);
			}

			for (Bid b : bidEntities) {
				if (users.size() > 0) {
					bids.add(new UserBid(b, bidMap.get(b.getUserId())));
				}
			}

			Collections.sort(bids, new BidComparator());
			userBids.setTotalBids(bids.size());
			userBids.setBids(bids);
			userBids.setRequestId(requestId);
		}
		return userBids;
	}

	@SuppressWarnings("unchecked")
	public List<UserBid> getUserBids(int requestId, BidStatus bidStatus) {
		List<UserBid> bids = new ArrayList<UserBid>();
		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = null;
		if (bidStatus != null) {
			query = session.createQuery("from Bid where requestId= :rid and status= :status");
			query.setParameter("rid", requestId);
			query.setParameter("status", bidStatus.getId());
		} else {
			query = session.createQuery("from Bid where requestId= :rid");
			query.setParameter("rid", requestId);
		}

		List<Bid> bidEntities = query.list();

		if (bidEntities != null && bidEntities.size() > 0) {
			Map<Long, User> bidMap = new HashMap<Long, User>();
			for (Bid b : bidEntities) {
				bidMap.put(b.getUserId(), null);
			}

			Query userQuery = session.createQuery("from User where userid in (:uid)");
			userQuery.setParameterList("uid", Arrays.asList(bidMap.keySet().toArray()));
			List<User> users = userQuery.list();

			for (User u : users) {
				bidMap.put(u.getUserid(), u);
			}

			for (Bid b : bidEntities) {
				if (users.size() > 0) {
					bids.add(new UserBid(b, bidMap.get(b.getUserId())));
				}
			}

			Collections.sort(bids, new BidComparator());
		}
		return bids;
	}

	// Sort by Rating, Price, Date
	class BidComparator implements Comparator<UserBid> {
		@Override
		public int compare(UserBid o1, UserBid o2) {
			int i = Double.compare(o1.getUser().getRating(), o2.getUser().getRating());
			if (i != 0)
				return i;

			i = Double.compare(o1.getAmount(), o2.getAmount());
			if (i != 0)
				return i;

			return o1.getBidDate().compareTo(o2.getBidDate());
		}
	}

}