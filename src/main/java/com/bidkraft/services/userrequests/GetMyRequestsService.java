package com.bidkraft.services.userrequests;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.Request;
import com.bidkraft.enums.RequestStatus;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.responses.MyRequestsResponse;
import com.bidkraft.responses.UserRequestResponse;
import com.bidkraft.services.KraftService;

@Component
@SuppressWarnings("unchecked")
public class GetMyRequestsService implements KraftService<MyRequestsResponse> {


	@Override
	public MyRequestsResponse service(KraftRequest request) throws KraftException {
		MyRequestsResponse result = new MyRequestsResponse();
		List<Request> userRequests = new ArrayList<Request>();
		if (request.getEntities().containsKey(ServiceKeys.GETUSERREQUESTS)) {

			Map<String, String> requestMap = (Map<String, String>) request.getEntities()
					.get(ServiceKeys.GETUSERREQUESTS);

			Long userid = Long.parseLong(requestMap.get("userid"));
			Session session = HibernateUtil.getSessionFactory().openSession();
			Query query = session.createQuery("from Request where userid= :uid");
			query.setParameter("uid", userid);

			userRequests = query.list();

			GetRequestInfoService requestInfoService = new GetRequestInfoService();
			if (userRequests != null) {
				for (Request r : userRequests) {
					UserRequestResponse response = requestInfoService.getRequestInfo(r);
					if (response.request.getStatus() == RequestStatus.WAITING_FOR_BIDS.getId()) {
						result.addActiveRequests(response);

					} else if (response.request.getStatus() == RequestStatus.AWAITING_PAYMENT.getId() || response.request.getStatus() == RequestStatus.PAID.getId()) {
						result.addAcceptedRequests(response);

					} else if (response.request.getStatus() == RequestStatus.COMPLETED.getId()) {
						result.addCompletedRequests(response);

					}
				}
			}
		}
		
		return result;
	}
	
	public List<UserRequestResponse> getUserRequests(List<Request> requests){
		List<UserRequestResponse> result = new ArrayList<UserRequestResponse>();
		
		
		
		return result;
	}
}
