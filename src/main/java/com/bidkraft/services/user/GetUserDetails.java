package com.bidkraft.services.user;

import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.User;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.services.KraftService;

@Component
public class GetUserDetails implements KraftService<User> {

	@Override
	public User service(KraftRequest request) throws KraftException {

		User userdetails = new User();
		if (request.getEntities().containsKey(ServiceKeys.GETUSERDETAILS)) {
			@SuppressWarnings("unchecked")
			Map<String, String> requestMap = (Map<String, String>) request.getEntities()
					.get(ServiceKeys.GETUSERDETAILS);

			Long userid = Long.parseLong(requestMap.get("userid"));
			userdetails = getUser(userid);
		}
		return userdetails;
	}

	@SuppressWarnings("unchecked")
	public User getUser(Long userid) {
		List<User> userdetails;
		Session session = HibernateUtil.getSessionFactory().openSession();

		Query query = session.createQuery("from User where userid= :userid");
		query.setParameter("userid", userid);

		userdetails = query.list();
		return userdetails.get(0);
	}

}
