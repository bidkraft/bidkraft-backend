package com.bidkraft.services.user;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.User;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.services.KraftService;

@Component
public class UpdateTokenForPushService implements KraftService<User> {

	@Autowired
	private GetUserDetails userDetailsProvider;

	@Override
	public User service(KraftRequest request) throws KraftException {
		User user = new User();
		if (request.getEntities().containsKey(ServiceKeys.UPDATETOCKEN)) {
			@SuppressWarnings("unchecked")
			Map<String, String> usermap = (Map<String, String>) request.getEntities().get(ServiceKeys.UPDATETOCKEN);

			long userid = Long.parseLong(usermap.get("usermap"));
			
			String iosToken = "";
			String andriodToken = "";
			if(usermap.containsKey("iostoken") && StringUtils.isNotBlank(usermap.get("iostoken"))) {
				iosToken = usermap.get("iostoken");
			}
			if(usermap.containsKey("andriodtoken") && StringUtils.isNotBlank(usermap.get("andriodtoken"))) {
				andriodToken = usermap.get("andriodtoken");
			}
			user = userDetailsProvider.getUser(userid);

			if (StringUtils.isNotBlank(user.getiOSToken())) {
				user.setiOSToken(user.getiOSToken() + "," + iosToken);
			}

			Session session = HibernateUtil.getSessionFactory().openSession();

			Query query = session.createQuery("update User set ios_token = :iostoken, andriod_token = :andriodtoken where userid = :userid");
			query.setParameter("iostoken", iosToken);
			query.setParameter("andriodtoken",andriodToken);
			query.setParameter("userid", userid);
			int result = query.executeUpdate();
			System.out.println(result);
			session.flush();
		}
		return user;
	}

}
