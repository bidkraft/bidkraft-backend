package com.bidkraft.services.user;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.elasticsearch.IndexAPI;
import com.bidkraft.entities.User;
import com.bidkraft.entities.Verification;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.services.KraftService;
import com.bidkraft.util.JsonMapper;
import com.bidkraft.util.KraftUtil;
import com.google.common.base.Joiner;

@Component
public class RegistrationService implements KraftService<User> {
	
	@Autowired
	JsonMapper jsonMapper;
	
	@Autowired
	IndexAPI indexAPI;

	@Override
	public User service(KraftRequest request) throws KraftException {	
		User user = new User();
		Verification verification = new Verification();

		if (request.getEntities().containsKey(ServiceKeys.REGISTRATION)) {
			@SuppressWarnings("unchecked")
			Map<String, String> usermap = (Map<String, String>) request.getEntities().get(ServiceKeys.REGISTRATION);

			System.out.println("In Registration Service:" + request.getEntities().get(ServiceKeys.LOGIN));

			System.out.println("User info" + usermap.get("password"));
			
			//TODO: Check if the emails exists already
			
			
			user.setEmail(usermap.get("email"));
			user.setPassword(usermap.get("password"));
			
			if(usermap.containsKey("lat") && StringUtils.isNotBlank(usermap.get("lat"))) {
				user.setLatitude(Double.parseDouble(usermap.get("lat")));
			}
			
			if(usermap.containsKey("lon") && StringUtils.isNotBlank(usermap.get("lon"))) {
				user.setLongitude(Double.parseDouble(usermap.get("lon")));
			}
			
			user.setBgcstatus(ServiceKeys.BGC_INTIAL);
			
			if(usermap.containsKey("rating") && StringUtils.isNotBlank(usermap.get("rating"))) {
				user.setRating(Double.parseDouble(usermap.get("rating")));
			}
			
			if(usermap.containsKey("profile_image")) {
				user.setImage(usermap.get("profile_image").getBytes());
			}
			
			if(usermap.containsKey("ios_token") && StringUtils.isNotBlank(usermap.get("ios_token"))) {
				user.setiOSToken(usermap.get("ios_token"));
			}
			
			if(usermap.containsKey("andriod_token") && StringUtils.isNotBlank(usermap.get("andriod_token"))) {
				user.setiOSToken(usermap.get("andriod_token"));
			}

			Session session = HibernateUtil.getSessionFactory().openSession();
			session.beginTransaction();
			session.save(user);

			System.out.println("Generated User id:" + user.getUserid());

			if (user != null) {
				verification.setUserid(user.getUserid());
				verification.setToken(KraftUtil.generateVerificationToken());
				verification.setCreationDate(KraftUtil.getSQLDate());
				session.save(verification);
			}
			
			try {
				usermap.put("userid", String.valueOf(user.getUserid()));
				if (user.getLatitude() != 0.00d && user.getLongitude() != 0.00d) {
					usermap.put("location",
							Joiner.on(",").join(user.getLatitude(), user.getLongitude()));
				}
				indexAPI.indexUser(String.valueOf(user.getUserid()), jsonMapper.getObjectMapper().writeValueAsString(usermap));
			} catch (KraftException e) {
				throw e;
			} catch (Exception e) {
				throw new KraftException(e.getMessage());
			}
			
			session.getTransaction().commit();
		}
		return user;
	}
}
