package com.bidkraft.services.userjobs;

import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.Collection;
import com.bidkraft.entities.Job;
import com.bidkraft.enums.CollectionStatus;
import com.bidkraft.enums.JobStatus;
import com.bidkraft.enums.RequestStatus;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.services.KraftService;

@Component
public class PostPayService implements KraftService<Job> {

	@Override
	public Job service(KraftRequest request) throws KraftException {
		Job jobobj = new Job();
		if (request.getEntities().containsKey(ServiceKeys.ABID)) {
			Map<String, String> usermap = (Map<String, String>) request.getEntities().get(ServiceKeys.POSTPAY);

			long bidId = Long.parseLong(usermap.get("bidid"));
			long reqId = Long.parseLong(usermap.get("reqid"));
			int buyerId = Integer.parseInt(usermap.get("buyerid"));
			double bidAmount = Double.parseDouble(usermap.get("bidamount"));
			String transactionId = usermap.get("transactionid");

			// Create a Job
			jobobj.setRequest_id(reqId);
			jobobj.setBid_id(bidId);
			jobobj.setStatus(JobStatus.FUNDED.getId());
			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction transcation = session.beginTransaction();

			try {
				session.save(jobobj);

				// Create a collection
				Collection collectionobj = new Collection();
				collectionobj.setAmount(bidAmount);
				collectionobj.setBuyerid(buyerId);
				collectionobj.setCollectionstatus(CollectionStatus.COLLECTED_FROM_BUYER.getId());
				collectionobj.setJobid(jobobj.getJob_id());
				collectionobj.setTransactionid(transactionId);
				session.save(collectionobj);

				// Update Request
				Query query2 = session.createQuery(
						"update Request set status = :status," + " jobid = :jobid" + " where requestid = :reqid");
				query2.setParameter("status", RequestStatus.PAID.getId());
				query2.setParameter("jobid", jobobj.getJob_id());
				query2.setParameter("reqid", reqId);
				query2.executeUpdate();

				// Update Job
				Query query3 = session
						.createQuery("update Job set collection_id = :collectionid," + " where id = :jobid");
				query3.setParameter("collection_id", collectionobj.getCollectionid());
				query3.setParameter("jobid", jobobj.getJob_id());
				query3.executeUpdate();

			} catch (Exception e) {
				transcation.rollback();
			} finally {
				transcation.commit();
			}

			System.out.println("Generated job id:" + jobobj.getJob_id());
		}
		return jobobj;
	}

}
