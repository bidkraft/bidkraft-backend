package com.bidkraft.services.userjobs;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.entities.Job;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.services.KraftService;

@Component
@SuppressWarnings("unchecked")
public class GetUserJobsService implements KraftService<List<Job>> {
	
	@Override
	public List<Job> service(KraftRequest request) throws KraftException {
		List<Job> userJobs = new ArrayList<Job>();
		if (request.getEntities().containsKey(ServiceKeys.GETUSERBIDS)) {

			Map<String, String> requestMap = (Map<String, String>) request.getEntities().get(ServiceKeys.GETUSERBIDS);
			Long userId = Long.parseLong(requestMap.get("userid"));
			userJobs = getJobById(userId);
		}
		return userJobs;
	}
	
	public List<Job> getJobById(Long jobId) {
		List<Job> userJobs = new ArrayList<Job>();

		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query; 
				
//		if(jobstatus != null) {	
//			query=session.createQuery("from Job where userId= :userid and jobStatus= :jobstatus");
//			//query.setParameter("userid", userId);
//			query.setParameter("status", jobstatus.getId());
//		} else {
			query=session.createQuery("from Job where id= :jobid");
			query.setParameter("jobid", jobId);
//		}

		userJobs = query.list();
		return userJobs;
	}

}
