package com.bidkraft.controller;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonAutoDetect.Visibility;
import org.codehaus.jackson.annotate.JsonMethod;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bidkraft.constants.ServiceKeys;
import com.bidkraft.exception.KraftException;
import com.bidkraft.model.KraftRequest;
import com.bidkraft.model.KraftResponse;
import com.bidkraft.notifications.CreateBidNotificationService;
import com.bidkraft.notifications.NotificationService;
import com.bidkraft.pushnotifications.PushNotificationService;
import com.bidkraft.services.KraftService;
import com.bidkraft.services.user.GetUserDetails;
import com.bidkraft.services.user.LoginService;
import com.bidkraft.services.user.RegistrationService;
import com.bidkraft.services.user.UpdateTokenForPushService;
import com.bidkraft.services.user.VerificationService;
import com.bidkraft.services.userbids.AcceptBidService;
import com.bidkraft.services.userbids.CreateBidService;
import com.bidkraft.services.userbids.GetMyBidsService;
import com.bidkraft.services.userbids.UpdateBidService;
import com.bidkraft.services.userjobs.PostPayService;
import com.bidkraft.services.userrequests.CreateRequestService;
import com.bidkraft.services.userrequests.GetAvailableRequestsService;
import com.bidkraft.services.userrequests.GetMyRequestsService;
import com.bidkraft.services.userrequests.GetRequestBidsService;
import com.bidkraft.services.userrequests.GetRequestInfoService;
import com.bidkraft.services.userreviews.CreateReviewService;
import com.bidkraft.services.userreviews.GetUserReviewsService;

import com.bidkraft.util.JsonMapper;

@Component
@Path("/do")
public class GenericServiceController {

	@Autowired
	JsonMapper jsonMapper;

	@Autowired
	LoginService loginService;

	@Autowired
	RegistrationService registrationService;

	@Autowired
	VerificationService verificationService;

	@Autowired
	CreateRequestService createRequestService;

	@Autowired
	CreateBidService createBidService;

	@Autowired
	AcceptBidService acceptBidService;

	@Autowired
	UpdateBidService updateBidService;

	@Autowired
	GetAvailableRequestsService getAvailableRequestsService;

	@Autowired
	GetMyRequestsService getMyRequestsService;

	@Autowired
	GetMyBidsService getMyBidsService;

	@Autowired
	GetRequestBidsService getRequestBidsService;

	@Autowired
	GetRequestInfoService getRequestInfoService;

	@Autowired
	CreateReviewService createReviewService;

	@Autowired
	GetUserDetails getUserDetails;

	@Autowired
	GetUserReviewsService getUserReviewsService;

	@Autowired
	PostPayService postPayService;

	@Autowired
	UpdateTokenForPushService updateTokenForPushService;
	
	private Map<String, KraftService<?>> serviceMap = new HashMap<String, KraftService<?>>();
	private Map<String, NotificationService> notificationsMap = new HashMap<String, NotificationService>();
	final static Logger LOGGER = LogManager.getLogger(GenericServiceController.class);

	public void initServiceMap() {
		if (serviceMap.isEmpty()) {
			serviceMap.put(ServiceKeys.LOGIN,  loginService);
			serviceMap.put(ServiceKeys.REGISTRATION,  registrationService);
			serviceMap.put(ServiceKeys.VERIFICATION,  verificationService);
			serviceMap.put(ServiceKeys.CREQ,  createRequestService);
			serviceMap.put(ServiceKeys.CBID,  createBidService);
			serviceMap.put(ServiceKeys.ABID,  acceptBidService);
			serviceMap.put(ServiceKeys.UBID,  updateBidService);
			serviceMap.put(ServiceKeys.GETAVAILABLEREQUESTS,  getAvailableRequestsService);
			serviceMap.put(ServiceKeys.GETUSERREQUESTS,  getMyRequestsService);
			serviceMap.put(ServiceKeys.GETUSERBIDS,  getMyBidsService);
			serviceMap.put(ServiceKeys.GETREQUESTBIDS,  getRequestBidsService);
			serviceMap.put(ServiceKeys.GETREQUESTINFO,  getRequestInfoService);
			serviceMap.put(ServiceKeys.CREV,  createReviewService);
			serviceMap.put(ServiceKeys.GETUSERDETAILS,  getUserDetails);
			serviceMap.put(ServiceKeys.GETUSERREVIEWS,  getUserReviewsService);

			serviceMap.put(ServiceKeys.GETMYREQUESTS,  getAvailableRequestsService);
			serviceMap.put(ServiceKeys.GETMYBIDS,  getMyBidsService);

			serviceMap.put(ServiceKeys.FINISHJOB,  postPayService);

			serviceMap.put(ServiceKeys.UPDATETOCKEN,  updateTokenForPushService);

			LOGGER.info("Generic Service ControllerInitialized");
		}
		
		if (notificationsMap.isEmpty()) {
			notificationsMap.put(ServiceKeys.CREATEBIDNOTIFICATION,  new CreateBidNotificationService());
			
		}
	}

	@POST
	@Path("/login")
	public Response doLogin(String request) {
		return executeWebRequest(request);
	}

	@POST
	@Path("/registration")
	@Produces("application/json")
	public Response doRegistration(String request) {
		return executeWebRequest(request);
	}

	private Response executeWebRequest(String request) {
		// TODO: Change the response of execute services to Response instead of
		// string
		initServiceMap();
		String result = executeServices(request);
		if (result.contains("error")) {
			return Response.status(200).entity(result).build();
		} else {
			return Response.status(200).entity(result).build();
		}
	}

	@POST
	@Path("/getavailreq")
	@Produces("application/json")
	public Response getAvailableRequests(String request) {
		return executeWebRequest(request);
	}

	@POST
	@Path("/creq")
	@Consumes("application/json")
	@Produces("application/json")
	public Response doRequest(String request) {
		return executeWebRequest(request);
	}

	@POST
	@Path("/getuserreq")
	@Produces("application/json")
	public Response doUserRequest(String request) {
		return executeWebRequest(request);
	}

	@POST
	@Path("/cbid")
	@Produces("application/json")
	public Response doBidding(String request) {
		return executeWebRequest(request);
	}

	@POST
	@Path("/abid")
	@Produces("application/json")
	public Response doABid(String request) {
		return executeWebRequest(request);
	}

	@POST
	@Path("/finishjob")
	@Produces("application/json")
	public Response doFinishJob(String request) {
		return executeWebRequest(request);
	}

	@POST
	@Path("/ubid")
	@Produces("application/json")
	public Response doUBid(String request) {
		return executeWebRequest(request);
	}

	@GET
	@Path("/hello")
	@Produces("application/json")
	public String doHelloBidKraft() {
		return "Hello BidKraft";
	}

	@POST
	@Path("/getuserbids")
	@Produces("application/json")
	public Response doUserBid(String request) {
		return executeWebRequest(request);
	}

	@POST
	@Path("/getreqbids")
	@Produces("application/json")
	public Response doRequestBids(String request) {
		return executeWebRequest(request);
	}

	@POST
	@Path("/getreqinfo")
	@Produces("application/json")
	public Response getRequestInfo(String request) {
		return executeWebRequest(request);
	}

	@POST
	@Path("/getuserdetails")
	@Produces("application/json")
	public Response dogetuserdetails(String request) {
		return executeWebRequest(request);
	}

	@POST
	@Path("/crev")
	@Produces("application/json")
	public Response doCreateReview(String request) {
		return executeWebRequest(request);
	}

	@POST
	@Path("/getuserreviews")
	@Produces("application/json")
	public Response doGetUSerReviews(String request) {
		return executeWebRequest(request);
	}

	@POST
	@Path("/updatetoken")
	@Produces("application/json")
	public Response updatetoken(String request) {
		return executeWebRequest(request);
	}

	@GET
	@Path("/verification/{userid}/{tokenid}")
	@Produces("application/json")
	public Response doUserVerification(@PathParam("userid") String userid, @PathParam("tokenid") String tokenid)
			throws URISyntaxException, JsonParseException, JsonMappingException, IOException {
		initServiceMap();
		String request = "{\"masterKey\":\"VERIFICATION\",\"entities\":{\"VERIFICATION\":{\"userid\":\"" + userid
				+ "\",\"tokenid\":\"" + tokenid + "\"}},\"addons\":[]}";
		String response = executeServices(request);
		response = response.replace("\"", "");
		response = response.replace("{status:success,keys:[VERIFICATION],entities:{VERIFICATION:", "");
		response = response.replace("}}", "");

		URI location = new URI("http://localhost:8080/BidKraftServices/kraftverify.jsp?msg=" + response);
		return Response.temporaryRedirect(location).build();
		// return Response.status(200).entity(executeServices(request)).build();
	}

	private String executeServices(String request) {
		String output = "";
		KraftResponse kraftResponse = new KraftResponse("success");
		KraftRequest kraftRequest = null;
		System.out.println("Request :" + request);
		try {
			kraftRequest = jsonMapper.getObjectMapper().readValue(request, KraftRequest.class);
			// call the master service
			if (kraftRequest.getMasterKey() != null) {
				Object response = serviceMap.get(kraftRequest.getMasterKey()).service(kraftRequest);
				notificationsMap.get(kraftRequest.getMasterKey()).sendNotification(response);
				if (response != null) {
					kraftResponse.getKeys().add(kraftRequest.getMasterKey());
					kraftResponse.getEntities().put(kraftRequest.getMasterKey(), response);
					// call the addon services
					if (kraftRequest.getAddons() != null) {
						for (int i = 0; i < kraftRequest.getAddons().size(); i++) {
							Object responseAddon = serviceMap.get(kraftRequest.getAddons().get(i))
									.service(kraftRequest);
							kraftResponse.getEntities().put(kraftRequest.getAddons().get(i), responseAddon);
						}
					}
				}
			}
			output = jsonMapper.getObjectMapper().writeValueAsString(kraftResponse);
		} catch (KraftException e) {
			buildError(kraftResponse, kraftRequest, e.getErrorMessage());
			try {
				output = jsonMapper.getObjectMapper().writeValueAsString(kraftResponse);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} catch (Exception e) {
			buildError(kraftResponse, kraftRequest, e.getCause().getMessage());
			try {
				output = jsonMapper.getObjectMapper().writeValueAsString(kraftResponse);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		return output;
	}

	private KraftResponse executeServicesKraftRequest(String request) {
		KraftResponse kraftResponse = new KraftResponse("success");
		KraftRequest kraftRequest = null;
		System.out.println("Request :" + request);
		try {
			kraftRequest = jsonMapper.getObjectMapper().readValue(request, KraftRequest.class);
			// call the master service
			if (kraftRequest.getMasterKey() != null) {
				Object response = serviceMap.get(kraftRequest.getMasterKey()).service(kraftRequest);
				if (response != null) {
					kraftResponse.getKeys().add(kraftRequest.getMasterKey());
					kraftResponse.getEntities().put(kraftRequest.getMasterKey(), response);
					// call the addon services
					if (kraftRequest.getAddons() != null) {
						for (int i = 0; i < kraftRequest.getAddons().size(); i++) {
							Object responseAddon = serviceMap.get(kraftRequest.getAddons().get(i))
									.service(kraftRequest);
							kraftResponse.getEntities().put(kraftRequest.getAddons().get(i), responseAddon);
						}
					}
				}
			}
		} catch (KraftException e) {
			// e1.printStackTrace();
			buildError(kraftResponse, kraftRequest, e.getErrorMessage());
		} catch (Exception e) {
			buildError(kraftResponse, kraftRequest, e.getMessage());
		}
		return kraftResponse;
	}

	private void buildError(KraftResponse kraftResponse, KraftRequest kraftRequest, String message) {
		if (kraftRequest != null) {
			kraftResponse.getKeys().add(kraftRequest.getMasterKey());
			kraftResponse.getEntities().put(kraftRequest.getMasterKey(), message);
		} else {
			kraftResponse.getEntities().put("error", message);
		}
		kraftResponse.setStatus("error");
	}

	
	private String buildErrorFromException(Exception e) {
		StringBuilder sb = new StringBuilder();

		return sb.toString();
	}
}