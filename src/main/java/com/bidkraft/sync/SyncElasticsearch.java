package com.bidkraft.sync;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.bidkraft.elasticsearch.IndexAPI;
import com.bidkraft.entities.Request;
import com.bidkraft.entities.User;
import com.bidkraft.exception.KraftException;
import com.bidkraft.persistence.HibernateUtil;
import com.bidkraft.util.JsonMapper;
import com.google.common.base.Joiner;

@Component
public class SyncElasticsearch {
	
	@Autowired
	IndexAPI indexAPI;
	
	@Autowired
	JsonMapper jsonMapper;

	public static void main(String[] args) {
		
		ApplicationContext context =
		    	  new ClassPathXmlApplicationContext(new String[] {"applicationContext.xml"});
		
		SyncElasticsearch cust = (SyncElasticsearch)context.getBean("syncElasticsearch");

		try {
			//syncRequests();
			cust.syncUsers();
		} catch (KraftException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void syncRequests() throws KraftException {

		// Mapping :
		// {"servicerequest":{"mappings":{"request":{"properties":{"address":{"type":"string"},"category":{"type":"string"},"description":{"type":"string"},"end_date":{"type":"string"},"fields":{"type":"string"},"jobid":{"type":"long"},"lat":{"type":"string"},"location":{"type":"geo_point"},"lon":{"type":"string"},"query":{"properties":{"match_all":{"type":"object"}}},"requestId":{"type":"string"},"requestid":{"type":"string"},"start_date":{"type":"string"},"status":{"type":"long"},"summary":{"type":"string"},"title":{"type":"string"},"userid":{"type":"string"},"winningbidid":{"type":"long"}}}}}}

		List<Request> requests = new ArrayList<Request>();
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			Query query = session.createQuery("from Request");
			requests = query.list();

			for (Request reqobj : requests) {
				writeToES(reqobj);
			}

		} catch (KraftException e) {
			throw e;
		} catch (Exception e) {
			throw new KraftException(e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	public void syncUsers() throws KraftException {

		List<User> users = new ArrayList<User>();
		try {
			Session session = HibernateUtil.getSessionFactory().openSession();
			Query query = session.createQuery("from User");
			users = query.list();

			for (User user : users) {
				writeToES(user);
			}

		} catch (KraftException e) {
			throw e;
		} catch (Exception e) {
			throw new KraftException(e.getMessage());
		}
	}

	public void writeToES(int requestId) throws Exception {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Query query = session.createQuery("from Request where id= :reqid");
		query.setParameter("reqid", requestId);

		List<Request> req = query.list();

		writeToES(req.get(0));

	}

	public void writeToES(User user) throws Exception {
		Map<String, String> usermap = jsonMapper.getObjectMapper().convertValue(user, Map.class);
		usermap.put("userid", String.valueOf(user.getUserid()));
		if (user.getLatitude() != 0.00d && user.getLongitude() != 0.00d) {
			usermap.put("location",
					Joiner.on(",").join(user.getLatitude(), user.getLongitude()));
		}
		indexAPI.indexUser(String.valueOf(user.getUserid()), jsonMapper.getObjectMapper().writeValueAsString(usermap));
	}

	public void writeToES(Request reqobj) throws Exception {

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		ObjectMapper m = new ObjectMapper();
		m.setDateFormat(df);
		Map<String, String> usermap = m.convertValue(reqobj, Map.class);

		if (usermap.containsKey("start_date")) {
			if (usermap.get("start_date").length() < 13) {
				usermap.put("start_date", df.format(reqobj.getStart_date()));
			}
		}
		if (usermap.containsKey("end_date")) {
			usermap.put("end_date", df.format(reqobj.getEnd_date()));
		}
		// usermap.put("requestid", String.valueOf(reqobj.getRequestid()));
		if (reqobj.getLatitude_request() != 0 && reqobj.getLongitude_request() != 0) {
			usermap.put("location", Joiner.on(",").join(reqobj.getLatitude_request(), reqobj.getLongitude_request()));
		}

		indexAPI.indexRequest(String.valueOf(reqobj.getRequestid()), m.writeValueAsString(usermap));
	}

}
