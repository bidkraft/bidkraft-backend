package com.bidkraft.enums;

public enum CollectionStatus {
	COLLECTED_FROM_BUYER(1, "COLLECTED_FROM_BUYER");
	
	private int id;
	private String name;
	
	private CollectionStatus(int id, String name){
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

}
