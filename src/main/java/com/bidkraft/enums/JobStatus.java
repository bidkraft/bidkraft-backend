package com.bidkraft.enums;

public enum JobStatus {

	CANCELLED(0, "CANCELLED"),
	INITIATED(1, "INITIATED"),
	FUNDED(2,"FUNDED"),
	DISTRIBUTED(3, "DISTRIBUTED"),
	FINISHED(4,"FINISHED");

	
	private int id;
	private String name;
	
	private JobStatus(int id,String name){
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

}
