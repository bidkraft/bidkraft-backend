package com.bidkraft.enums;

public enum RequestStatus {
	
	WAITING_FOR_BIDS(1, "WAITING_FOR_BIDS"),
	//(2, "WAITING_FOR_BIDS"),
	AWAITING_PAYMENT(3, "AWAITING_PAYMENT"),
	PAID(4, "PAID"),
	COMPLETED(5, "COMPLETED"),
	CANCELLED(6, "CANCELLED");
	
	private int id;
	private String name;
	
	private RequestStatus(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

}
