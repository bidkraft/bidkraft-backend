package com.bidkraft.enums;

public enum BidStatus {
	
	ACTIVE_BID(1, "ACTIVEBID"),
	CANCELLED_BID(2, "CANCELLED BID"),
	WINING_BID(3, "WINING BID");
	
	private int id;
	private String name;
	
	private BidStatus(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}
	
}
