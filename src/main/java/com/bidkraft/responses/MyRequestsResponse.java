package com.bidkraft.responses;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;


public class MyRequestsResponse {
	
	@JsonProperty("activeRequests")
	private List<UserRequestResponse> activeRequests;
	
	@JsonProperty("acceptedRequests")
	private List<UserRequestResponse> acceptedRequests;
	
	@JsonProperty("completedRequests")
	private List<UserRequestResponse> completedRequests;
	
	public MyRequestsResponse() {
		activeRequests = new ArrayList<UserRequestResponse>();
		acceptedRequests = new ArrayList<UserRequestResponse>();
		completedRequests = new ArrayList<UserRequestResponse>();
	}
		
	
	public void addActiveRequests(UserRequestResponse activeRequest) {
		if(activeRequests == null) {
			activeRequests = new ArrayList<UserRequestResponse>();
		}
		this.activeRequests.add(activeRequest);
	}
	
	public void addAcceptedRequests(UserRequestResponse acceptedRequest) {
		if(acceptedRequests == null) {
			acceptedRequests = new ArrayList<UserRequestResponse>();
		}
		this.acceptedRequests.add(acceptedRequest);
	}
	
	public void addCompletedRequests(UserRequestResponse completedRequest) {
		if(completedRequests == null) {
			completedRequests = new ArrayList<UserRequestResponse>();
		}
		this.completedRequests.add(completedRequest);
	}
	
	public void setAcceptedRequests(List<UserRequestResponse> acceptedRequests) {
		this.acceptedRequests = acceptedRequests;
	}
	public void setCompletedRequests(List<UserRequestResponse> completedRequests) {
		this.completedRequests = completedRequests;
	}

}
