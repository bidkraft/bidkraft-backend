package com.bidkraft.responses;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

public class MyBidsResponse {

	@JsonProperty("activeBids")
	private List<UserRequestResponse> activeBids = new ArrayList<UserRequestResponse>();
	
	@JsonProperty("acceptedBids")
	private List<UserRequestResponse> acceptedBids = new ArrayList<UserRequestResponse>();
	
	@JsonProperty("completedBids")
	private List<UserRequestResponse> completedBids = new ArrayList<UserRequestResponse>();
		
	
	public void addActiveBids(UserRequestResponse activeBid) {
		if(activeBids == null) {
			activeBids = new ArrayList<UserRequestResponse>();
		}
		this.activeBids.add(activeBid);
	}
	
	public void addAcceptedBids(UserRequestResponse acceptedBid) {
		if(acceptedBids == null) {
			acceptedBids = new ArrayList<UserRequestResponse>();
		}
		this.acceptedBids.add(acceptedBid);
	}
	
	public void addCompletedBids(UserRequestResponse completedBid) {
		if(completedBids == null) {
			completedBids = new ArrayList<UserRequestResponse>();
		}
		this.completedBids.add(completedBid);
	}
	
	public void setAcceptedBids(List<UserRequestResponse> acceptedBids) {
		this.acceptedBids = acceptedBids;
	}
	public void setCompletedBids(List<UserRequestResponse> completedBids) {
		this.completedBids = completedBids;
	}
	
	
}
