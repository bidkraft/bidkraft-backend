package com.bidkraft.elasticsearch;

import java.net.UnknownHostException;

import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bidkraft.exception.KraftException;

@Component
public class IndexAPI {
	
	@Autowired
	ElasticsearchClient elasticsearchClient;
	
	public boolean indexRequest(String requestId, String request) throws KraftException {
		try {
			Client esClient = elasticsearchClient.getClient();
			IndexResponse response = esClient.prepareIndex("servicerequest", "request").setId(requestId).setSource(request).get();
			if (response.isCreated()) {
				return true;
			} else
				return false;

		} catch (UnknownHostException e) {
			throw new KraftException("IndexAPI Exception " + e.getMessage());
		}
	}
	
	public boolean indexUser(String userId, String request) throws KraftException {
		try {
			Client esClient = elasticsearchClient.getClient();
			IndexResponse response = esClient.prepareIndex("servicerequest", "user").setId(userId).setSource(request).get();
			if (response.isCreated()) {
				return true;
			} else
				return false;

		} catch (UnknownHostException e) {
			throw new KraftException("IndexAPI Exception " + e.getMessage());
		}
	}

}
