package com.bidkraft.pushnotifications;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bidkraft.entities.User;
import com.bidkraft.services.user.GetUserDetails;

@Component
public class PushNotificationService {

	@Autowired
	iOSPushNotificatonProvider iOSPushNotificatonProvider;

	@Autowired
	AndriodPushNotificationProvider andriodPushNotificationProvider;

	@Autowired
	GetUserDetails userDetailsProvider;
	
	public PushNotificationService() {
		if(userDetailsProvider == null) {
			userDetailsProvider = new GetUserDetails();
		}
		if(iOSPushNotificatonProvider == null) {
			iOSPushNotificatonProvider = new iOSPushNotificatonProvider();
		}
		
	}
	
	public boolean pushToiOS(String iOSToken, String title, String body) {
		return iOSPushNotificatonProvider.push(iOSToken, title, body);
	}
	
	public boolean pushToAndriod(String andriodToken, String title, String body) {
		return andriodPushNotificationProvider.push(andriodToken, title, body);
	}

	public boolean push(String userId, String title, String body) {
		User user = userDetailsProvider.getUser(Long.valueOf(userId));

		if (StringUtils.isNotBlank(user.getiOSToken())) {
			String[] iOSTokens = user.getiOSToken().split(",");
			for (String iOSToken : iOSTokens) {
				iOSPushNotificatonProvider.push(iOSToken, title, body);
			}
		}
		if (StringUtils.isNotBlank(user.getAndriodToken())) {
			String[] andriodTokens = user.getAndriodToken().split(",");
			for (String andriodToken : andriodTokens) {
				andriodPushNotificationProvider.push(andriodToken, title, body);
			}
		}
		return false;
	}

}
