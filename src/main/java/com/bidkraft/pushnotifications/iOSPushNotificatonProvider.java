package com.bidkraft.pushnotifications;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

import javax.net.ssl.SSLException;

import org.springframework.stereotype.Component;

import com.relayrides.pushy.apns.ApnsClient;
import com.relayrides.pushy.apns.ApnsClientBuilder;
import com.relayrides.pushy.apns.ClientNotConnectedException;
import com.relayrides.pushy.apns.PushNotificationResponse;
import com.relayrides.pushy.apns.util.ApnsPayloadBuilder;
import com.relayrides.pushy.apns.util.SimpleApnsPushNotification;
import com.relayrides.pushy.apns.util.TokenUtil;

import io.netty.util.concurrent.Future;

@Component
public class iOSPushNotificatonProvider implements PushNotificationProvider {
	
	public static void main(String[] args) {
		//iOSPushNotificatonProvider ser = new iOSPushNotificatonProvider();
		
	} 

	@Override
	public boolean push(String deviceToken, String title, String body) {	
		boolean result = false;

		SimpleApnsPushNotification pushNotification = buildPushNotification(deviceToken, title, body);
		Future<PushNotificationResponse<SimpleApnsPushNotification>> sendNotificationFuture;
		
		ApnsClient client = null;
		Future<Void> connectFuture;
		try {
			client = getApnsClient();
			connectFuture = client.connect(ApnsClient.DEVELOPMENT_APNS_HOST);
			connectFuture.await();
		} catch (SSLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			sendNotificationFuture = client.sendNotification(pushNotification);
		    final PushNotificationResponse<SimpleApnsPushNotification> pushNotificationResponse =
		            sendNotificationFuture.get();

		    if (pushNotificationResponse.isAccepted()) {
		    	result = true;
		        System.out.println("Push notification accepted by APNs gateway.");
		    } else {
		        System.out.println("Notification rejected by the APNs gateway: " +
		                pushNotificationResponse.getRejectionReason());

		        if (pushNotificationResponse.getTokenInvalidationTimestamp() != null) {
		            System.out.println("\t…and the token is invalid as of " +
		                pushNotificationResponse.getTokenInvalidationTimestamp());
		        }
		    }
		} catch (final ExecutionException e) {
		    System.err.println("Failed to send push notification.");
		    e.printStackTrace();

		    if (e.getCause() instanceof ClientNotConnectedException) {
		        System.out.println("Waiting for client to reconnect…");
		        try {
					client.getReconnectionFuture().await();
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		        System.out.println("Reconnected.");
      
		    }
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public ApnsClient getApnsClient() throws SSLException, IOException {
		//Get file from resources folder
		ClassLoader classLoader = getClass().getClassLoader();
		
		final ApnsClient apnsClient = new ApnsClientBuilder()
		        .setClientCredentials(new File(classLoader.getResource("iospushnotification/Certificates.p12").getFile()), "bidkraft")
		        .build();
		return apnsClient;
	}
	
	public SimpleApnsPushNotification buildPushNotification(String deviceToken, String title, String body) {
		final SimpleApnsPushNotification pushNotification;
		final ApnsPayloadBuilder payloadBuilder = new ApnsPayloadBuilder();
		payloadBuilder.setAlertTitle(title);
		payloadBuilder.setAlertBody(body);
		final String payload = payloadBuilder.buildWithDefaultMaximumLength();
		//12ce9be7d98acaaa045cf6e3d9e2c990ec19a4608e90befff2a0f42b521c5380
		final String token = TokenUtil.sanitizeTokenString(deviceToken);
		pushNotification = new SimpleApnsPushNotification(token, "com.bidkraft", payload);
		return pushNotification;
	}

}
