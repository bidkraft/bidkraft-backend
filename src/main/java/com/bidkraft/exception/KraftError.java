package com.bidkraft.exception;

public enum KraftError {
	DB_NOT_AVAILABLE(1, "Database down", KraftErrorType.DATABASE, true),
	ES_NOT_AVAILABLE(2, "Elasticsearch down", KraftErrorType.ES, true),
	JSON_EXCEPTION(3, "Json exception", KraftErrorType.SYSTEM, false),
	
	UNKNOWN(99, "Unknown error", KraftErrorType.SYSTEM, false);
	//TODO : List all the errors
	
	private int id;
	private String name;
	private KraftErrorType errorType;
	private boolean retriable;
	
	
	private KraftError(int id, String name, KraftErrorType errorType, boolean retriable){
		this.id = id;
		this.name = name;
		this.errorType = errorType;
		this.retriable = retriable;
	}
	
	public String getName() {
		return name;
	}

}
