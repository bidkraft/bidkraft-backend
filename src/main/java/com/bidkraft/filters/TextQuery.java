package com.bidkraft.filters;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

public class TextQuery implements Query {
	
	public String queryField;
	public String queryText;
	
	public TextQuery() {
		
	}
	
	public TextQuery(String field, String query){
		this.queryField = field;
		this.queryText = query;
	}
	
	public QueryBuilder getQuery(){
		if(this.queryField.isEmpty()) {
		return QueryBuilders.multiMatchQuery(this.queryText, "title", "description");
		} else {
			return QueryBuilders.multiMatchQuery(this.queryText, this.queryField);
		}
		
	}

}
