package com.bidkraft.filters;

import org.elasticsearch.common.geo.GeoDistance;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

public class GeoQuery implements Query{
	
	public double distance;
	public double longitude;
	public double latitude;
	
	public GeoQuery() {
		
	}
	
	public GeoQuery(double distance, double lon, double lat) {
		this.distance = distance;
		this.longitude = lon;
		this.latitude = lat;
	}
	
	
	public QueryBuilder getQuery(){
		QueryBuilder qb = QueryBuilders.geoDistanceQuery("location")  
			    .point(latitude, longitude)                                 
			    .distance(distance, DistanceUnit.MILES)         
			    .optimizeBbox("memory")                         
			    .geoDistance(GeoDistance.ARC);  
		
		return qb;
	}

}
