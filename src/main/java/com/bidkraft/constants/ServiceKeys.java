package com.bidkraft.constants;

public class ServiceKeys {
	
	public static final String LOGIN="LOGIN";
	public static final String REGISTRATION="REGISTRATION";
	public static final String VERIFICATION="VERIFICATION";
	public static final String CREQ="CREQ";//REQUEST 
	public static final String CBID="CBID";//CREATE Bid 
	public static final String CREV="CREV";//CREATE USER REVIEW
	
	public static final String ABID="ABID";//ACCEPT BID
	public static final String UBID="UBID";//UPDATE BID
	
	public static final String GETAVAILABLEREQUESTS="GETAVAILABLEREQ"; 
	public static final String GETUSERREQUESTS="GETUSERREQ"; 
	public static final String GETUSERBIDS="GETUSERBID";
	public static final String GETUSERJOBS="GETUSERJOBS";
	public static final String GETREQUESTBIDS="GETREQUESTBID";
	public static final String GETUSERDETAILS="GETUSERDETAILS";
	public static final String GETUSERREVIEWS="GETUSERREVIEWS";
	public static final String GETREQUESTINFO="GETREQUESTINFO";
	public static final String BGC_INTIAL="INITIALIZED";
	
	public static final String GETMYREQUESTS="GETMYREQUESTS";
	public static final String GETMYBIDS="GETMYBIDS";
	
	public static final String POSTPAY="POSTPAY";
	public static final String FINISHJOB="FINISHJOB";
	
	public static final String UPDATETOCKEN="UPDATETOCKEN";
	public static final String CREATEBIDNOTIFICATION="CREATEBIDNOTIFICATION";
}
